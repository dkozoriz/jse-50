package ru.t1.dkozoriz.tm.command.task;

import ru.t1.dkozoriz.tm.dto.request.task.TaskListClearRequest;

public final class TaskListClearCommand extends AbstractTaskCommand {

    public TaskListClearCommand() {
        super("task-clear", "delete all tasks.");
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getEndpointLocator().getTaskEndpoint().taskListClear(new TaskListClearRequest(getToken()));
    }

}