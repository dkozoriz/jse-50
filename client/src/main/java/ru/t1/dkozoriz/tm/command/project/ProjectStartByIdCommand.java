package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectStartByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    public ProjectStartByIdCommand() {
        super("project-start-by-id", "start project by id.");
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        getEndpointLocator().getProjectEndpoint().projectStartById(new ProjectStartByIdRequest(getToken(),id));
    }

}