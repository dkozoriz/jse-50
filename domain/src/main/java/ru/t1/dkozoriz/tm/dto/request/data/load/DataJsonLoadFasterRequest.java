package ru.t1.dkozoriz.tm.dto.request.data.load;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataJsonLoadFasterRequest extends AbstractUserRequest {

    public DataJsonLoadFasterRequest(@Nullable final String token) {
        super(token);
    }

}